import Vue from 'vue'
import VueRouter from 'vue-router'

//importanto componentes para as rotas
import Home from './views/Home.vue'
import Cadastro from './views/Cadastro.vue'
import Cliente from './components/Cliente.vue'
import RSenha from './views/RSenha.vue'
import Config from './views/Config.vue'
import Perfil from './views/Perfil.vue'
import Evento from './views/Evento.vue'
import Resumo from './views/Resumo.vue'

Vue.use(VueRouter);

//rotas criadas
export default new VueRouter({
    mode:'history',
    routes:[
        {
            path:'/',
            name: 'cliente',
            component: Cliente
        },
        {
           path:'/home',
           name: 'home',
           component: Home 
        },
        {
            path:'/cadastro',
            name: 'cadastro',
            component: Cadastro
        },
        {
            path:'/rsenha',
            name: 'rsenha',
            component: RSenha
        },
        {
            path:'/config',
            name:'config',
            component: Config
        },
        {
            path:'/perfil',
            name:'perfil',
            component: Perfil
        },
        {
            path:'/resumo',
            name:'resumo',
            component: Resumo
        },
        {
            path:'/eventos',
            name: 'eventos',
            component: Evento
        }
        
    ]
})
